// Smooth Scroll with Native JS
document.querySelectorAll('.navbar .nav-link').forEach(enlace => {
	enlace.addEventListener('click', (e) => {
		e.preventDefault();
		document.querySelector(enlace.getAttribute('href') ).scrollIntoView({
			behavior: 'smooth',
			block: 'start'
		});
	});
});

// Navbar Sticky Colour
window.onscroll = (e) => {
	const scroll = window.scrollY;

	const header = document.querySelector('#navegacion-principal');

	if(scroll > 300) {
		header.classList.add('bg-success');
	} else {
		header.classList.remove('bg-success');
	}
}

// Count Down
$(document).ready(function() {
	$('#fecha').countdown('2019/12/22', function (event) {
		console.log(event);
		$(this).html(event.strftime('<span> %D </span> Days <span> %H </span> Hours <span> %M </span> Minutes <span> %S </span> Seconds '));
	});
});
